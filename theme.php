<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_Foundation extends Theme {

    public $name = 'Foundation';
    public $author = 'Jos&eacute; Rico';
    public $author_website = 'http://www.joserico.com/';
    public $website = 'http://www.joserico.com/';
    public $description = 'Foundation 4 template for PyroCMS.';
    public $version = '1.0.0';
	public $options = array(
		'background' => array(
			'title'         => 'Background',
			'description'   => 'Choose the default background for the theme.',
			'default'       => 'noise',
			'type'          => 'select',
			'options'       => 'white=White|grey=Grey|noise=Noise',
			'is_required'   => true
		),
		'show_breadcrumbs' 	=> array(
			'title'         => 'Do you want to show breadcrumbs?',
			'description'   => 'If selected it shows a string of breadcrumbs at the top of the page.',
			'default'       => 'yes',
			'type'          => 'radio',
			'options'       => 'yes=Yes|no=No',
			'is_required'   => true
		),
	);
	
}

/* End of file theme.php */